//
//  PostCell.swift
//  LikeInsta
//
//  Created by Pavel on 12/02/2017.
//  Copyright © 2017 TapTop. All rights reserved.
//

// создаем этот CLASS для работы с ячейками UICollectionView

import UIKit

class PostCell: UICollectionViewCell {
    
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var unlikeBtn: UIButton!
    
    
    @IBAction func likePressed(_ sender: Any) {
    }
    @IBAction func unlikePressed(_ sender: Any) {
    }
    
}
