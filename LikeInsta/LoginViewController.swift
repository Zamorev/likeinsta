//
//  LoginViewController.swift
//  LikeInsta
//
//  Created by Pavel on 30/01/2017.
//  Copyright © 2017 TapTop. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var pwField: UITextField!
    @IBOutlet weak var authStatus: UILabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func loginPressed(_ sender: Any) {
        guard emailField.text != "", pwField.text != "" else {
//                return // тут можно вписать обработку ошибки
            authStatus.text = "неправильный логин или пароль"
            return
        }
        FIRAuth.auth()?.signIn(withEmail: emailField.text!, password: pwField.text!, completion: { (user, error) in
            
            if let error = error {
                print("\(error.localizedDescription)") // публикация ошибки
            }
            
            if let user = user {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "usersVC")
                self.authStatus.text = ""
                
                self.present(vc, animated: true, completion: nil)
            }
        })
        
    }

}
