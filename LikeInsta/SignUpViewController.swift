//
//  SignUpViewController.swift
//  LikeInsta
//
//  Created by Pavel on 30/01/2017.
//  Copyright © 2017 TapTop. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var comPwField: UITextField!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var nextLbl: UILabel!
    
    let picker = UIImagePickerController()
    var userStorage: FIRStorageReference! // для хранения данных о зарегистрированных пользователях
    var ref: FIRDatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // слушает связку с UIImagePickerControllerDelegate
        picker.delegate = self
        
        let storage = FIRStorage.storage().reference(forURL: "gs://likeinsta-a2982.appspot.com") // определяет адрес для хранения
        
        ref = FIRDatabase.database().reference()
        userStorage = storage.child("users") // данные для первого раздела содержимого storage


    }

    
    @IBAction func selectImagePressed(_ sender: Any) {
        picker.allowsEditing = true
        // выбор откуда грузить фото
        picker.sourceType = .photoLibrary
        
        present(picker, animated: true, completion: nil)
    
    }
    
    // проверка на завершение выбора файла
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        // проверка на случай крашей (защита)
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.imageView.image = image
            nextBtn.isHidden = false
        }
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func nextPressed(_ sender: Any) {
        
        // проверка на заполнение
        guard nameField.text != "", emailField.text != "", passwordField.text != "", comPwField.text != "" else {
            return
        }
        
        // провека на правильно ввода пароля (совпадение)
        if passwordField.text == comPwField.text {
            
            // так как проверка пройдена, то далее авторизация
            FIRAuth.auth()?.createUser(withEmail: emailField.text!, password: passwordField.text!, completion: { (user, error) in
                
                if let error = error {
                    print(error.localizedDescription)
                }
                
                if let user = user {
                    
                    let changeRequest = FIRAuth.auth()!.currentUser!.profileChangeRequest()
                    changeRequest.displayName = self.nameField.text // отображение имени
                    changeRequest.commitChanges(completion: nil)
                    
                    // создание информации о загрузке фото в storage с именем user.uid.jpg
                    let imageRef = self.userStorage.child("\(user.uid).jpg")
                    
                    // информация про сам файл с потерей качества
                    let data = UIImageJPEGRepresentation(self.imageView.image!, 0.5)
                    
                    // непосредственно загрузка
                    let uploadTask = imageRef.put(data!, metadata: nil, completion: { (metadata, err) in
                        
                        // проверка на ошибки
                        if err != nil {
                            print(err!.localizedDescription)
                        }
                        imageRef.downloadURL(completion: { (url, error3) in
                            if error3 != nil {
                                print(error3!.localizedDescription)
                            }
                            
                            if let url = url {
                                
                                let userInfo: [String : Any] = ["uid" : user.uid,
                                                                "full name" : self.nameField.text!,
                                                                "urlToImage" : url.absoluteString]
                                
                                // создание в Firebase уникальных (user.uid) папок для хранения данных, которые мы создали чуть ранее
                                self.ref.child("users").child(user.uid).setValue(userInfo)
                                
                                
                                // связь 
                                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "usersVC")
                                self.present(vc, animated: true, completion: nil)
                            }
                        })
                    })
                    
                    uploadTask.resume()
                    
                }
                
            })
            
        } else {
            print("Password doesn't match")
        }
    }
}
