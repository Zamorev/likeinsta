//
//  FeedViewController.swift
//  LikeInsta
//
//  Created by Pavel on 12/02/2017.
//  Copyright © 2017 TapTop. All rights reserved.
//

import UIKit
import Firebase

// работа с данными
class FeedViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var screenLblNeedToFollow: UILabel!
    
    // создаем переменную с указанием постов
    var posts = [Post]()
    var following = [String]() // список на кого подписан / String - user ID
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // запрашиваем функцию fetchPosts
        fetchPosts()
    }

    func fetchPosts(){
        
        // взаимодействие с базой
        let ref = FIRDatabase.database().reference()
        
        // собираем пользователей, на которых подписан текущий юзер
        ref.child("users").queryOrderedByKey().observeSingleEvent(of: .value, with: { snapshot in
            
            // получаем всех юзеров
            let users = snapshot.value as! [String : AnyObject] // String - инедтификатор юзера, AnyObject - все внутри этого юзера
            
            // цикл для всех юзеров
            for (_, value) in users {
                // извлекаем в цикле данные
                if let uid = value["uid"] as? String {
                    
                    // сравниваем с uid текущего пользователя
                    if uid == FIRAuth.auth()?.currentUser?.uid {
                        
                        // проверяем подписан ли он на кого-нибудь
                        if let followingUsers = value["following"] as? [String : String] {
                            
                            // если он подписан на кого-то
                            self.screenLblNeedToFollow.isHidden = true
                            
                            for (_,user) in followingUsers {
                                self.following.append(user)
                            }
                        }
                        // если ни на кого не подписан, то показывать свои же посты
                        self.following.append(FIRAuth.auth()!.currentUser!.uid)
                        
                        self.screenLblNeedToFollow.isHidden = false
                    
                        // далее идем смотреть все посты
                        ref.child("posts").queryOrderedByKey().observeSingleEvent(of: .value, with: { (snap) in  // snap - это как snapshot, но чтобы не повторялся
                        
                            let postsSnap = snap.value as! [String : AnyObject]
                            
                            // цикл между всеми постами и проверка если uid этого юзера совпадает с теми, на кого подписан. если не совпадает, то показать этот пост.
                            
                            for (_,post) in postsSnap {
                                if let userID = post["userID"] as? String {
                                    
                                    // проверка входит ли этот userID в число Following
                                    for each in self.following {
                                        if each == userID {
                                            
                                            // далее создаем пустой пост, в который вставляем данные
                                            let postNumber = Post()
                                            if let author = post["author"] as? String, let likes = post["likes"] as? Int, let pathToImage = post["pathToImage"] as? String, let postID = post["postID"] as? String {
                                                
                                                postNumber.author = author
                                                postNumber.likes = likes
                                                postNumber.pathToImage = pathToImage
                                                postNumber.postID = postID
                                                postNumber.userID = userID
                                                
                                                self.posts.append(postNumber)
                                            }
                                        }
                                    }
                                    
                                    self.collectionView.reloadData()
                                    
                                }
                            }
                            
                            
                        })
                        
                    }
                }
            }
            
        })
        
        ref.removeAllObservers()
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.posts.count // берем количество постов. по умолчанию = 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // для каждой ячейки свой пост
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "postCell", for: indexPath) as! PostCell // работа по идентификатору
        
        // код для загрузки всех данных про пост
        cell.postImage.downloadImage(from: self.posts[indexPath.row].pathToImage)
        cell.authorLabel.text = self.posts[indexPath.row].author
        cell.likeLabel.text = "\(self.posts[indexPath.row].likes!) Likes"
        
        return cell
    }

    @IBAction func btnBackPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
