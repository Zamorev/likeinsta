//
//  UploadViewController.swift
//  LikeInsta
//
//  Created by Pavel on 10/02/2017.
//  Copyright © 2017 TapTop. All rights reserved.
//

import UIKit
import Firebase

class UploadViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var previewImage: UIImageView!
    @IBOutlet weak var postBtn: UIButton!
    @IBOutlet weak var selectBtn: UIButton!
    
    var picker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // обращается к себе
       picker.delegate = self
        
    }
    
    
    // проверка завершения выбора фото
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        // связка с отредактированным избражением, так как при нажатии на кнопку selectPressed можно редактировать изображение
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            // после получения изображения (if = true)
            self.previewImage.image = image
            selectBtn.isHidden = true
            postBtn.isHidden = false
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func selectPressed(_ sender: Any) {
        
        // редактирование изображения
        picker.allowsEditing = true
        
        // поддержка фото
        picker.sourceType = .photoLibrary
        
        self.present(picker, animated: true, completion: nil)
        
        
    }

    @IBAction func postPressed(_ sender: Any) {
        
        // вызываем из AppDelegate созданное нами уведомление
        AppDelegate.instance().showActivityIndicator()
        
        // получает uid текущего пользователя
        let uid = FIRAuth.auth()!.currentUser!.uid
        
        // ссылка с базой данных
        let ref = FIRDatabase.database().reference()
        
        // доступ в хранилище для хранения данных
        let storage = FIRStorage.storage().reference(forURL: "gs://likeinsta-a2982.appspot.com")
        
        // создание уникального ID поста.
        let key = ref.child("posts").childByAutoId().key // создаем для Database папку "posts", на содержимое которого и будем ссылаться
        
        // создаем связь с хранилищем
        let imageRef = storage.child("posts").child(uid).child("\(key).jpg") // создаем файл JPG c уникальным именем
        
        // нужно конвертировать изображение в данные
        let data = UIImageJPEGRepresentation(self.previewImage.image!, 0.6)
        
        
        // после конвертации в данные, можно загружать на сервер
        let uploadTask = imageRef.put(data!, metadata: nil) { (metadata, error) in
            
            
            // если все хорошо, то проверка на ошибки
            if error != nil {
                print(error!.localizedDescription)
                
                // выключаем индикатор загрузки
                AppDelegate.instance().dismissActivityIndicator()
                return
            }
            
            // загрузка URL на изображение
            imageRef.downloadURL(completion: { (url, error) in
                if let url = url { // если мы получаем URL
                    let feed = ["userID" : uid,
                                "pathToImage" : url.absoluteString,
                                "likes" : 0,
                                "author" : FIRAuth.auth()!.currentUser!.displayName!,
                    "postID" : key] as [String : Any]
                    
                    let postFeed = ["\(key)" : feed] // создаем уникальный ID и вставляем в него содержимое FEED
                    
                    ref.child("posts").updateChildValues(postFeed)
                    
                    // выключаем индикатор загрузки
                    AppDelegate.instance().dismissActivityIndicator()
                    
                    self.dismiss(animated: true, completion: nil)
                    
                }
            })
        }
        
        uploadTask.resume()
        
        // все написанное выше загружает фото на сервер и формирует данные для дальнейшего использования.
        
        
    }
    @IBAction func btnBackPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
