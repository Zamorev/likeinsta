//
//  UsersViewController.swift
//  LikeInsta
//
//  Created by Pavel on 31/01/2017.
//  Copyright © 2017 TapTop. All rights reserved.
//

// создан для работы с таблицей users

import UIKit
import Firebase

class UsersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableview: UITableView!
    
    var user = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // функция вызова данных для отображения
        retrieveUsers()
        
        // операция привязки ниже сделана на main.storyboard
        // self.tableview.delegate = self
        //self.tableview.dataSource = self

    }
    
    // извлечение user
    func retrieveUsers() {
        
        // обращение к Firebase, определение числа юзеров и их имена
        let ref = FIRDatabase.database().reference()
        
        // так как в базе child является users
        ref.child("users").queryOrderedByKey().observeSingleEvent(of: .value, with: { snapshot in
        
            let users = snapshot.value as! [String : AnyObject]
            self.user.removeAll() // очистка списка перед запуском LOOP ниже
            
            // получение всех user ID, проверка их всех на совпадение с моим user ID, тогда не включать в список
            for (_, value) in users {
                if let uid = value["uid"] as? String {
                    
                    // строчка сопоставления userID с моим из Firebase
                    if uid != FIRAuth.auth()!.currentUser!.uid {
                        let userToShow = User()
                        if let fullName = value["full name"] as? String, let imagePath = value["urlToImage"] as? String {
                            userToShow.fullName = fullName // эту переменную создали и проверили строкой выше
                            userToShow.imagePath = imagePath
                            userToShow.userID = uid
                            
                            self.user.append(userToShow) //увеличение количества строк на еще одну
                        }
                    }
                }
                
            }
            
            self.tableview.reloadData()
            
        })
        
        ref.removeAllObservers()
    }
  
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // связка строки и класса UserCell
        let cell = tableview.dequeueReusableCell(withIdentifier: "userCell", for: indexPath) as! UserCell
        
        // каждой ячейке свое имя.
        // [indexPath.row].fullName возвращает имя из массива [indexPath.row]
        cell.nameLabel.text = self.user[indexPath.row].fullName
        cell.userID = self.user[indexPath.row].userID
        
        cell.userImage.downloadImage(from: self.user[indexPath.row].imagePath)
        
        checkFollowing(indexPath: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // создаем метод определения количества строк
        return user.count ?? 0 // если user.count не существует, то 0 (не ошибка чтобы)
    }
    
    // опредеяет связь с событиями по нажатиям на строки
    // в данном случае будет проверка на follow пользователя
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let uid = FIRAuth.auth()!.currentUser!.uid
        let ref = FIRDatabase.database().reference()
        let key = ref.child("users").childByAutoId().key
        
        var isFollower = false
        
        ref.child("users").child(uid).child("following").queryOrderedByKey().observeSingleEvent(of: .value, with: { snapshot in
        
            if let following = snapshot.value as? [String : AnyObject] {
                for (key2, value) in following {
                    if value as! String == self.user[indexPath.row].userID {
                        isFollower = true
                    
                        // удаление значение из Firebase
                        // удаление following
                        ref.child("users").child(uid).child("following/\(key2)").removeValue()
                        
                        // удаление follower
                        ref.child("users").child(self.user[indexPath.row].userID).child("followers/\(key2)").removeValue()
                        
                        self.tableview.cellForRow(at: indexPath)?.accessoryType = .none
                    }
                }
            }
            
            // follow as user has no followers
            if !isFollower {
                
                // создаем Dictionary
                let following = ["following/\(key)" : self.user[indexPath.row].userID]
                let followers = ["followers/\(key)" : uid]
                
                ref.child("users").child(uid).updateChildValues(following)
                ref.child("users").child(self.user[indexPath.row].userID).updateChildValues(followers)
                
                // помечать при подписке
                self.tableview.cellForRow(at: indexPath)?.accessoryType = .checkmark
            }
            
        })
        
        ref.removeAllObservers()
    }
    
    // проверка на подписку при авторизации
    func checkFollowing(indexPath : IndexPath) {
        let uid = FIRAuth.auth()!.currentUser!.uid
        let ref = FIRDatabase.database().reference()
        
        ref.child("users").child(uid).child("following").queryOrderedByKey().observeSingleEvent(of: .value, with: { snapshot in
            
            if let following = snapshot.value as? [String : AnyObject] {
                for (_, value) in following {
                    
                    if value as! String == self.user[indexPath.row].userID {
                        self.tableview.cellForRow(at: indexPath)?.accessoryType = .checkmark
                    }
                    
                }
            }
        })
        ref.removeAllObservers()
    }
    
    
    @IBAction func logOutPressed(_ sender: Any) {
    }


}



// все imageView будут использовать это расширение
extension UIImageView {
    func downloadImage(from imgURL: String!) {
        let url = URLRequest(url: URL(string: imgURL)!)
        let task = URLSession.shared.dataTask(with: url) {
            (data, response, error) in
        
            if error != nil {
                print(error!.localizedDescription)
            }
            
            DispatchQueue.main.async {
                self.image = UIImage(data: data!)
            }
        
        }
        task.resume()
    }
}




