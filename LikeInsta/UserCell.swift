//
//  UserCell.swift
//  LikeInsta
//
//  Created by Pavel on 31/01/2017.
//  Copyright © 2017 TapTop. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!

    // связка с UID
    // если нажать 1 раз, то подписываешься на него, если еще раз, то отписываешься
    
    var userID: String!

}
